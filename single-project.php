<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package bhs_construction
 */

get_header();
?>

	<div id="primary" class="content-area">
		<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
          
	      <div class="projectfeature" style="background: url('<?php echo $backgroundImg[0]; ?>') no-repeat;">
	         <header class="entry-header">
	        <h1 class="entry-title"><?php the_title(); ?></h1>
	         </header>
	      </div>  

		<main id="main" class="site-main">	
	

		
		<?php
		while ( have_posts() ) :
			the_post();
			
			echo '<div class="projectslideshow">';
			$slideshow = get_field('slideshow');
			$size = 'full';
			if( $slideshow ){
			echo '<div class="projectslider owl-carousel owl-theme">';
				foreach( $slideshow as $image ) {
					echo '<div class="slideitem" style="background-image: url(' . $image['url'] . ')">';
					echo '</div>';
				}
				
				echo '</div>';
			
			?>
			
			<script>
				$(document).ready(function(){
					$('.projectslider').owlCarousel({
							loop: true,
							items: 1,
							margin: 0,
							autoplay: true,
							dots: true,
							nav: true,
							//navText: ['<i class="fal fa-angle-left"></i>', '<i class="fal fa-angle-right"></i>'],
							navText: ['<span class="fa-stack fa-1x cool-span"><i aria-hidden="true" class="fa fa-circle fa-stack-1x" style="mix-blend-mode: hard-light;"><i aria-hidden="true" class="fa fa-angle-left fa-stack-1x" style="color: gray;"></i></i></span>', '<span class="fa-stack fa-1x cool-span"><i aria-hidden="true" class="fa fa-circle fa-stack-1x" style="mix-blend-mode: hard-light;"><i aria-hidden="true" class="fa fa-angle-right fa-stack-1x" style="color: gray;"></i></i></span>'],
						});
						
						
					
					});
					</script>
					
					<?php
			}
			
			$project_video = get_field('project_video');
			if ($project_video) {
			echo '<div class="project_video">';
			$project_video_headline = get_field('project_video_headline');
			$project_video_excerpt = get_field('project_video_excerpt');
			$small_feature = get_field('small_feature');
			$small_feature_thumb = $small_feature['sizes'][ 'medium' ];
			echo '<div class="project_video_text">';
			echo '<h3>' . $project_video_headline . '</h3>';
			echo '<p>' . $project_video_excerpt . '</p>';
			echo '</div>';
			echo '<div class="project_video_link">';
			echo '<a class="videolink" href="' . $project_video . '"><img src="' . $small_feature_thumb .'"><span class="play-icon"></span></a>';
			echo '</div>';
			echo '</div>';
			}
			echo '</div>';
			//get_template_part( 'template-parts/content', get_post_type() );
echo '<div class="project_excerpt">';
the_field('project_excerpt');
echo '</div>';
echo '<div class="project-details">';
echo '<div class="project-details-left">';
echo '<h2>Project Features</h2>';
if( have_rows('features') ){ 
echo '<ul class="project-features">';
	while ( have_rows('features') ) { the_row(); 
		echo '<li>';
		the_sub_field('feature');
		echo '</li>';
	}
echo '</ul>';
}
echo '</div>';
echo '<div class="project-details-right">';
echo '<h2>Project Summary</h2>';
echo '<table cellspacing="0" border="0">';
if (get_field('cost')){
echo '<tr><td class="project-details-name">Project Cost</td><td class="project-details-value">$';
the_field('cost');
echo '</td></tr>';
}

if (get_field('area')){
echo '<tr><td class="project-details-name">Area</td><td class="project-details-value">';
the_field('area');
echo ' sq ft</td></tr>';
}

if (get_field('completion')){
echo '<tr><td class="project-details-name">Completion</td><td class="project-details-value">';
the_field('completion');
echo '</td></tr>';
}

if (get_field('budget')){
echo '<tr><td class="project-details-name">Budget</td><td class="project-details-value">';
the_field('budget');
echo '</td></tr>';
}

if (get_field('location')){
echo '<tr><td class="project-details-name">Location</td><td class="project-details-value">';
the_field('location');
echo '</td></tr>';
}

if (get_field('architect')){
echo '<tr><td class="project-details-name">Architect</td><td class="project-details-value">';
the_field('architect');
echo '</td></tr>';
}

echo '</table>';
echo '</div>';
echo '</div>';


//$next_post = get_next_post();
$next_post = get_adjacent_post();
if ( is_a( $next_post , 'WP_Post' ) ) {

	$nextid = $next_post->ID;
}
else {
	$recent_posts = wp_get_recent_posts(array(
        'numberposts' => 1,
        'post_status' => 'publish',
		'post_type'=>'project',
    ));
	$nextid = $recent_posts[0]['ID'];
	}
	
	$nextlink = get_permalink( $nextid );
	$nextitle = get_the_title( $nextid );
	$projectimage = get_the_post_thumbnail_url( $nextid, 'large' );
	echo '<div class="project-next-post">';
	echo '<div class="box" style="background-image: url(\'' . $projectimage . '\')">';
	echo '<div class="boxcontent"><a href="' . $nextlink . '">';
	echo '<div class="item"><span>Up Next:</span><h3>' . $nextitle .'</h3></div>';
	echo '</a></div>';
	echo '</div>';
	//the_post_navigation();
	//next_post_link();
	//next_post_link( '%link', 'Next post in category', TRUE );
	echo '</div>';
	
	
	
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
