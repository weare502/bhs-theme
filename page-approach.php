<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bhs_construction
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

		endwhile; // End of the loop.
		?>
		<div class="entry-content"> 
		<?php
		if( have_rows('approach_repeater') ){
			echo '<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="' . get_stylesheet_directory_uri() . '/js/jquery.rwdImageMaps.min.js"></script>
    <script>
  jQuery(document).ready(function( $ ) {
    $( "#approach-tabs" ).tabs();
	$("img[usemap]").rwdImageMaps();
	
  } );
  
  function vennHover(tabnum, popupnum){
  
	var popup = $("#approach-tabs-" + tabnum + " .approach-tab-popup").eq(popupnum);
	//alert("popup");
	//popup.show();
	var coords = $("#approach-tabs-" + tabnum + " area").eq(popupnum).attr("coords").split(",");
	//popup.css("left", parseInt(coords[0]) + 40 + "px");
	popup.css("left", parseInt(coords[0]) + 53 + "px");
	//popup.css("left", 0);
	popup.css("top", coords[1] + "px");
	//popup.css("top", 0);
	popup.show();
	
	}
	
	function vennLeave(num){
	$(".approach-tab-popup").hide();
	}
	
  </script>
  <div id="approach-tabs">
  <ul>';
  $x = 1;
  $y = 1;
 while ( have_rows('approach_repeater') ) { the_row();
	$tab_name = get_sub_field('tab_name');
	echo '<li><a href="#approach-tabs-'. $x . '">' . $tab_name . '</a></li>';
	$x++;
 }
 echo '</ul>';
 while ( have_rows('approach_repeater') ) { the_row();
	$name = get_sub_field('name');
	$info = get_sub_field('info');
	$ideal_projects = get_sub_field('ideal_projects');
	$venn = get_sub_field('venn');
	$imageurl = $venn['url'];
	$imageheight = $venn['height'];
	$imagewidth = $venn['width'];
	$pros_cons = get_sub_field('pros_&_cons');
	$popup_title = get_sub_field('popup_title');
	$popup_info = get_sub_field('popup_info');
	$popup_pros_cons = get_sub_field('popup_pros_&_cons');
	$hotspot = get_sub_field('hotspot');
	$venn_hotspots = get_sub_field('venn_hotspots');
	$popups = get_sub_field('popups');
	
	
	echo '<div id="approach-tabs-'. $y . '">
	<div class="approach-tab-top">
	<div class="approach-tab-info">
	';
	echo '<h3>' . $name . '</h3><p>' . $info . '</p>';
	echo '</div>
 <div class="approach-tab-ideal-projects">';
 if ($ideal_projects) {
	echo '<h3>Ideal Projects</h3>
	<ul>';
	foreach( $ideal_projects as $ide ){
			echo '<li><i class="fas fa-plus"></i> ' . $ide['project_title'] . '</li>';
		}
	echo '</ul>';
	}
	echo '</div></div>';
	echo '<div class="approach-tab-content">
 <div class="approach-tab-venn">';
 //echo '<img class="venn-diagram" src="' . $imageurl .'" usemap="#vennmap'. $y . '" height="'. $imageheight . '" width="'. $imagewidth . '" />';
 echo '<img class="venn-diagram" src="' . $imageurl .'" usemap="#vennmap'. $y . '" style="height: ' . $imageheight . 'px; width: '. $imagewidth . 'px;" />';
 
 if ($popups){
 echo '<map name="vennmap'. $y . '">';
 $popupnum = 0;
 foreach( $popups as $pop ){
	$coords = explode( ',', $pop['coordinates']);
	$coordsx1 = $coords[0] * ($imagewidth / 100);
	$coordsy1 = $coords[1] * ($imageheight / 100);
	echo '<area shape="circle" onmouseover="vennHover('. $y . ', ' . $popupnum . ')" onmouseout="vennLeave()" coords="' . $coordsx1 . ',' . $coordsy1 . ',15" href="javascript:void(0);" alt="popup">';
	$popupnum++;
 }
 echo '</map>';
 }
 /*
 $coords = explode( ',', $hotspot );
 $coordsx1 = $coords[0] * ($imagewidth / 100);
 $coordsy1 = $coords[1] * ($imageheight / 100);
 echo'
 <map name="vennmap'. $y . '">
  <area shape="circle" onmouseover="vennHover('. $y . ')" onmouseout="vennLeave()" coords="' . $coordsx1 . ',' . $coordsy1 . ',15" href="#" alt="popup">
</map>
 ';
 */
 if ($popups){
 echo '<div class="approach-tab-popups">';
	foreach( $popups as $pop ){
	$popup_title = $pop['popup_title'];
	$popup_info = $pop['popup_info'];
	$popup_pros_cons = $pop['pros_&_cons'];
	
	//echo '<button class="approach-tab-popup-button">poup</button>';
	echo '<div class="approach-tab-popup">';
	//echo '<i class="fas fa-caret-down"></i>';
	echo '<img class="approach-tab-popup-arrow" src="' . get_stylesheet_directory_uri() . '/leftarrow.png">';
	echo '<h3>' . $popup_title . '</h3><p>' . $popup_info . '</p>';
	
	if ($popup_pros_cons) {
	echo '<ul>';
	foreach( $popup_pros_cons as $poppros ){
	$pro_or_con = $poppros['pro_or_con'];
		if ($pro_or_con == 'Pro'){
				echo '<li><i class="fas fa-check-circle"></i> ' . $poppros['item'] . '</li>';
		}
		elseif ($pro_or_con == 'Con'){
				echo '<li><i class="fas fa-times-circle"></i> ' . $poppros['item'] . '</li>';
		}
	}
	echo '</ul>';
	}
	
	echo '</div>';
 }
 echo '</div>';
 }
 echo '</div>';
 echo '<div class="approach-tab-content-right">
 <div class="approach-tab-key">
 <h4>Key</h4>
 <div class="keycode">
 <span class="keyletter"><span class="approach-tab-key-letter">O</span> - Owner</span>  <span class="keyletter"><span class="approach-tab-key-letter">C</span> - Contractor</span>  <span class="keyletter"><span class="approach-tab-key-letter">A</span> - Architect</span>
 </div>
 </div>
 <div class="approach-tab-content-pro-cons">';
 
 if ($pros_cons) {
	echo '<h3>Pros & Cons</h3>
	<ul>';
	foreach( $pros_cons as $procon ){
	$pro_or_con = $procon['pro_or_con'];
		if ($pro_or_con == 'Pro'){
				echo '<li><i class="fas fa-check-circle"></i> ' . $procon['item'] . '</li>';
		}
		elseif ($pro_or_con == 'Con'){
				echo '<li><i class="fas fa-times-circle"></i> ' . $procon['item'] . '</li>';
		}
	}
	echo '</ul>';
	}
	echo '</div></div></div>';
	echo '</div>';
	
	$y++;
			}
 echo '</div>';
		}
		?>


 </div>
 
 
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
