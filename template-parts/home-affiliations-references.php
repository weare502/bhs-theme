<section id="affiliationsreferences">
	<div class="container">
		<div class="affiliationsreferencesheader item">
			<h3>Affiliations & References</h3>
			
			
		</div>
		<div class="affiliationsreferenceslogos item">
			<div class="affiliationslogo item">
				<?php
				/*
				// check if the repeater field has rows of data
				if( have_rows('logos','options') ):?>
				<ul id="ftlogos">
					
					<?php // loop through the rows of data
				    while ( have_rows('logos','options') ) : the_row();?>
				
				        <li>
				        <img src="<?php the_sub_field('logo_image');?> " />
				        </li>
				        
				   <?php endwhile;?>
				   
				</ul>
				<?php
				
				else :
				
				    // no rows found
				
				endif;
				*/
				$repeater = get_field( 'logos','options' );
				$random_rows = array_rand( $repeater, 6 );
				if ($repeater) {
				echo '<ul id="ftlogos">';
				if( is_array( $random_rows ) ){
					foreach( $random_rows as $random_row ){
						echo '<li><img src="' . $repeater[$random_row]['logo_image'] . '" /></li>';
					}
				}
				else {
					echo '<li><img src="' . $repeater[$random_rows]['logo_image'] . '/></li>';
				}
				echo '</ul>';
				}
				?>
			</div>
			<div class="viewall item">
				<a href="/affiliations-references/">View All</a>
			</div>
		</div>
	</div>
</section>