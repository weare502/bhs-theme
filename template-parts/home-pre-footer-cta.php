<div class="container">
	<section id="prefooterctas">
	<div class="prefooterctas item">
		<?php
	
		// check if the repeater field has rows of data
		if( have_rows('cta','options') ):?>
		<ul id="ftcta">
		
		 	<?php // loop through the rows of data
		    while ( have_rows('cta','options') ) : the_row();?>
		
				<a href="<?php the_sub_field('link');?>">
					<li style="background: url('<?php the_sub_field('background');?>') no-repeat center center;">	
					<div class="ctaheadline item">	        
				        <h3><?php the_sub_field('headline');?></h3> <i class="fas fa-long-arrow-alt-right"></i>
					</div>
					<div class="ctacontent item">
				        <?php the_sub_field('content');?>
					</div>
					</li>  
				</a> 
		        
		        			        			
		   <?php endwhile;?>
		</ul>
		<?php
		
		else :
		
		    // no rows found
		
		endif;
		
		?>
	</div>
	
</section>
</div>