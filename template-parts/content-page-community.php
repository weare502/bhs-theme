<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bhs_construction
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
	<div class="feature" style="background: url('<?php echo $backgroundImg[0]; ?>') no-repeat center center;">
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
	</div>
	
	<?php
	echo '<div class="communityslideshow">';
			$slideshow = get_field('community_gallery');
			$size = 'full';
			if( $slideshow ){
			echo '<div class="projectslider owl-carousel owl-theme">';
				foreach( $slideshow as $image ) {
					echo '<div class="slideitem" style="background-image: url(' . $image['url'] . ')">';
					echo '</div>';
				}
				
				echo '</div>';
			
			?>
			
			<script>
				$(document).ready(function(){
					$('.projectslider').owlCarousel({
							loop: true,
							items: 1,
							margin: 0,
							autoplay: true,
							dots: true,
							nav: true,
							//navText: ['<i class="fal fa-angle-left"></i>', '<i class="fal fa-angle-right"></i>'],
							navText: ['<span class="fa-stack fa-1x cool-span"><i aria-hidden="true" class="fa fa-circle fa-stack-1x" style="mix-blend-mode: hard-light;"><i aria-hidden="true" class="fa fa-angle-left fa-stack-1x" style="color: gray;"></i></i></span>', '<span class="fa-stack fa-1x cool-span"><i aria-hidden="true" class="fa fa-circle fa-stack-1x" style="mix-blend-mode: hard-light;"><i aria-hidden="true" class="fa fa-angle-right fa-stack-1x" style="color: gray;"></i></i></span>'],
						});
						
						
					
					});
					</script>
					
					<?php
			}
			echo '</div>';
	
	?>
	
	<!--<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<?php //bhs_construction_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'bhs_construction' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'bhs_construction' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
