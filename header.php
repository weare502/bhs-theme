<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bhs_construction
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,400i,600,600i,700,700i&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/8d9adf61e1.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://use.typekit.net/pxx3usc.css">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/owl/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/owl/owl.theme.default.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/owl/owl.carousel.min.js"></script>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/animate.css">
<?php the_field('header_scripts','options');?>
	<?php wp_head(); ?>
</head>
<script>
$(document).ready(function(){
	$('#side-navigation .menu .menu-item-has-children > a').after('<i class="fal fa-chevron-down" aria-hidden="true"></i>');
	$('#side-navigation .menu .menu-item-has-children ul').hide();
	$('#side-navigation .menu .menu-item-has-children i').on("click touch", function () {
				var is = $('#side-navigation .menu .menu-item-has-children i').not($(this).parents("li").find("i"));
				$(is).next("ul").slideUp();
				$(this).next("ul").slideToggle();

				$(is).removeClass('fa-chevron-up').addClass('fa-chevron-down');
				$(this).toggleClass('fa-chevron-up fa-chevron-down');
			});
	$('#page nav .menu-toggle').on("click touch", function () {
			$('body').toggleClass('side-nav-open');
			$('#side-navigation .menu .menu-item-has-children i').next("ul").slideUp();
			$('#side-navigation .menu .menu-item-has-children i').removeClass('fa-chevron-up').addClass('fa-chevron-down');

			});	
				$('#side-navigation .menu-toggle').on("click touch", function () {
			$('body').removeClass('side-nav-open');
			$('#side-navigation .menu .menu-item-has-children i').next("ul").slideUp();
			$('#side-navigation .menu .menu-item-has-children i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
			
			
			});
	

	
});

function sidenav(){
	$('body').toggleClass('side-nav-open');
	$('#side-navigation .menu .menu-item-has-children i').next("ul").slideUp();
	$('#side-navigation .menu .menu-item-has-children i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
}

</script>
<body <?php body_class(); ?>>
<div class="menu-container">
	<nav id="side-navigation" class="side-navigation">
		<button class="menu-toggle" aria-controls="side-menu" aria-expanded="false"><!--Exit Navigation<span class="menu-toggle-bar">|</span>--><span class="menu-toggle-x"><i class="fal fa-times"></i></span></button>

		<?php
		wp_nav_menu( array(
			'theme_location' => 'menu-1',
			'menu_id'        => 'primary-menu',
		) );
		?>
	</nav>
</div>
<div class="side-nav-open-container" onClick="sidenav()"></div>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'bhs_construction' ); ?></a>

	<header id="masthead" class="site-header container">
		<div class="site-branding">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php get_template_part( 'fragments/headerlogo');?></a>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i class="fal fa-bars"></i></button>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
			) );
			?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
