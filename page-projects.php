<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bhs_construction
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php
		while ( have_posts() ) {
			the_post();

			//get_template_part( 'template-parts/content', 'page' );
			} // End of the loop.
?>
<div class="projects-archive">
<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
<div class="projectfeature" style="background: url('<?php echo $backgroundImg[0]; ?>') no-repeat;">
<h1><em>Our</em> Projects</h1>
	<form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter">
	<?php
		if( $terms = get_terms( array( 'taxonomy' => 'projectcategory', 'orderby' => 'name' ) ) ) : 
 
			echo '<select id="categoryfilter" name="categoryfilter"><option value="">Filter By Industry</option>';
			foreach ( $terms as $term ) :
				echo '<option value="' . $term->term_id . '">' . $term->name . '</option>'; // ID of the category as the value of an option
			endforeach;
			echo '</select>';
		endif;
	?>
	<!--
	<input type="text" name="posts-per-page" placeholder="posts-per-page" value="4" />
	<button>Apply filter</button>
	-->
	<input type="hidden" name="action" value="projectsfilter">
	<input type="hidden" id="pageprojects" name="pageprojects" value="1">
</form>
</div>
<!--<button class="more-projects">more</button>-->
<div class="featured-projects">

<?php
		$args = array(
	'posts_per_page'	=> 12,
	//'posts_per_page'	=> 2,
	'post_type'		=> 'project',
	'post_status' => 'publish',
	);

$the_query = new WP_Query( $args );
if( $the_query->have_posts() ){
	$total_results = $the_query->found_posts;
	$pagesnum = $the_query->max_num_pages;
	while ( $the_query->have_posts() ) { $the_query->the_post();
		
		$pid = get_the_ID();
		$title = get_the_title();
		$permalink = get_permalink();
		$projectimage = get_the_post_thumbnail_url( $pid, 'large' );
		
		echo '<div class="box" style="background-image: url(\'' . $projectimage . '\')">';
		echo '<div class="boxout"></div>';
		echo '<div class="boxcontent"><a href="' . $permalink . '">';
		echo '<div class="item"><h3>' . $title .'</h3><hr></div>';
		echo '</a></div>';
		echo '</div>';
	}
	
}
wp_reset_postdata();

?>
</div>

<div class="more-projects">
				<a href="#">View More</a>
			</div>
			<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/js/chosen/chosen.min.css">
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/chosen/chosen.jquery.js"></script>
		<script>
		var pageprojects = 1;
		var canBeLoaded = true;
		
		 jQuery(document).ready(function( $ ) {
				//$('#filter').submit(function(){
		$('#categoryfilter').change(function(){
		var filter = $('#filter');
		pageprojects = 1;
		$('#pageprojects').val(pageprojects);
		$.ajax({
			url:filter.attr('action'),
			data:filter.serialize(), // form data
			type:filter.attr('method'), // POST
			beforeSend:function(xhr){
				filter.find('button').text('Processing...'); // changing the button label
			},
			success:function(data){
				filter.find('button').text('Apply filter'); // changing the button label back
				$('.featured-projects').html(data); // insert data
			}
		});
		return false;
	});
	
	$('.more-projects').on("click", function () {
	//$(window).scroll(function(){
	//var el = $('.projects-archive');
	//var bottomOffset = bottomOffset = 1000;
	//bottomOffset = el.offset().top + el.outerHeight();
	//footerOffset = $('footer').offset().top;
	//bottomOffset = $(window).height() + footerOffset;
	//if( $(document).scrollTop() > (  bottomOffset ) && canBeLoaded == true ){
				pageprojects++;
				$('#pageprojects').val(pageprojects);
				//$('#filter').submit();
				var filter = $('#filter');
		$.ajax({
			url:filter.attr('action'),
			data:filter.serialize(), // form data
			type:filter.attr('method'), // POST
			beforeSend:function(xhr){
				filter.find('button').text('Processing...');
				canBeLoaded = false;
			},
			success:function(data){
				filter.find('button').text('Apply filter');
				$('.featured-projects').html(data);
				canBeLoaded = true;
			}
		});
		return false;
				//}
				
			});
			
			$("#categoryfilter").chosen({
				disable_search: true,
				width: "300px",
				allow_single_deselect: true,
				inherit_select_classes: true,
   
			});
			$('.chosen-container-single .chosen-single div b').html('<i class="fas fa-caret-down"></i>');
	
	});
					</script>

</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
