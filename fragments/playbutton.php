<svg width="40px" height="41px" viewBox="0 0 40 41" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 59.1 (86144) - https://sketch.com -->
    <g id="Desktop-Page-Designs" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Group-7" transform="translate(0.000000, 0.822581)">
            <circle id="Oval" fill="#FFFFFF" cx="20" cy="20" r="20"></circle>
            <polygon id="Rectangle" fill="#D3232C" transform="translate(18.347107, 19.745349) rotate(-45.000000) translate(-18.347107, -19.745349) " points="21.7403412 14.7666185 23.3258381 24.7240798 13.3683768 23.1385829"></polygon>
        </g>
    </g>
</svg>