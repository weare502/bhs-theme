<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bhs_construction
 */

?>

	</div><!-- #content -->
</div><!-- #page -->

<?php

if( get_field('affiliations_&_references') == 'show' ) {
   get_template_part( 'template-parts/home-affiliations-references');
}

if( get_field('pre_footer_cta') == 'show' ) {
	get_template_part( 'template-parts/projects-pre-footer-cta');
}
;?>



	<footer id="colophon" class="site-footer">
		<div class="site-info container">
			<div class="column_one item">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php get_template_part( "fragments/footerlogo");?></a>
				
				<?php

				// check if the repeater field has rows of data
				if( have_rows('social_icons','options') ):?>
				<ul id="socialicons">
					
					<?php // loop through the rows of data
				    while ( have_rows('social_icons','options') ) : the_row();?>
				    <li>
				        <?php // display a sub field value ;?>
				        <a href="<?php the_sub_field('link');?>" target="_blank"><?php the_sub_field('icon');?></a>
				
				    </li>
				<?php
				    endwhile;?>
				</ul>
				<?php
				
				else :
				
				    // no rows found
				
				endif;
				
				?>
			</div>
			<div class="item coltwothree">
				<div class="column_two item">
				<?php the_field('footer_one','options');?>
				</div>
				<div class="column_three item">
					<?php the_field('footer_two','options');?>
				</div>
			</div>
			<div class="column_four item">
				<?php get_template_part( "fragments/buildtogetherfooter");?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>discuss-a-project"><h3>Discuss a Project</h3></a>
			</div>
		</div><!-- .site-info -->
		
		<div class="site-copy container">
			<div class="column_one item">
				<nav id="footer-navigation" class="ft-navigation">
					<?php
					wp_nav_menu( array(
						'theme_location' => 'footer-1',
						'menu_id'        => 'footer-menu',
					) );
					?>
				</nav><!-- #site-navigation -->
			</div>
			<div class="column_two item">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">&copy; <?php echo date('Y');?> BHS Construction</a>
			</div>
			<div class="column_three item">
				<a href="https://weare502.com/" target="_blank">Design & Development by <?php get_template_part( "fragments/502logo");?></a>
			</div>
		</div>
	</footer><!-- #colophon -->
<?php the_field('footer_scripts','options');?>
<?php wp_footer(); ?>

</body>
</html>
