<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bhs_construction
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

		endwhile; // End of the loop.
		?>
<div class="entry-content">

<?php
$num = 0;
if( have_rows('office_staff') ){
	echo '<h2>Office Staff</h2>
	<div class="staff-list">';
	while ( have_rows('office_staff') ) { the_row();
		$name = get_sub_field('name');
		$title = get_sub_field('title');
		$image = get_sub_field('image');
		$imageurl = wp_get_attachment_image_url( $image, 'large');
		$bio = get_sub_field('bio');
		$education = get_sub_field('education');
		$year = get_sub_field('year');

		echo '<div id="box" class="box" style="background-image: url(' . $imageurl . ')">
<div class="boxout"></div>';
echo '<div class="staff-list-name"><span><strong>' . $name . '</strong></span><br><span>' . $title . '</span><br/><span style="font-size: 0.875rem;">' . $year . '</span></div>';
echo '<div class="boxcontent">';
if ($bio || $education) {
	echo '<a href="#boxpopup' . $num . '" class="open-popup-link">';
}
echo '<div class="item"><h4>' . $name . '</h4>';
echo '<h5>' . $title . '</h5>';
if ($bio || $education) {
	echo '<div class="staff-more-info">More Info</div>';
}
echo '</div>';
if ($bio || $education) {
	echo '</a>';
}
echo '</div>';
echo '<div id="boxpopup' . $num . '" class="boxpopup mfp-hide">
	<div class="leftside">';
	echo '<img src="' . $imageurl . '">
	<h2>' . $name . '</h2>
	<h5>' . $title . '</h5>
	<h5>' . $year . '</h5>
	</div>';
	echo '<div class="rightside">';
	if ($education) {
	echo '<h4>Education</h3>
	<div class="staff-list-detail">';
	foreach( $education as $edu ){
			echo '<p>' . $edu['name'] . '</p>';
		}
	echo '</div>';
	}
	if ($bio) {
	echo '
	<h4>Biography</h3>
	<div class="staff-list-detail">' . $bio . '</div>';
	}
	echo '</div>
</div>
</div>
';
		$num++;
	}
	echo '</div>';
}

if( have_rows('superintendent_staff') ){
	echo '<h2>Superintendent Staff</h2>
	<div class="staff-list">';
	while ( have_rows('superintendent_staff') ) { the_row();
		$name = get_sub_field('name');
		$title = get_sub_field('title');
		$image = get_sub_field('image');
		$imageurl = wp_get_attachment_image_url( $image, 'large');
		$bio = get_sub_field('bio');
		$education = get_sub_field('education');
		$year = get_sub_field('year');

		echo '<div id="box" class="box" style="background-image: url(' . $imageurl . ')">
<div class="boxout"></div>';
echo '<div class="staff-list-name"><span><strong>' . $name . '</strong></span><br><span>' . $title . '</span><br/><span style="font-size: 0.875rem;">' . $year . '</span></div>';
echo '<div class="boxcontent">';
if ($bio || $education) {
	echo '<a href="#boxpopup' . $num . '" class="open-popup-link">';
}
echo '<div class="item"><h4>' . $name . '</h4>';
echo '<h5>' . $title . '</h5>';
if ($bio || $education) {
	echo '<div class="staff-more-info">More Info</div>';
}
echo '</div>';
if ($bio || $education) {
	echo '</a>';
}
echo '</div>';
echo '<div id="boxpopup' . $num . '" class="boxpopup mfp-hide">
	<div class="leftside">';
	echo '<img src="' . $imageurl . '">
	<h2>' . $name . '</h2>
	<h5>' . $title . '</h5>
	<h5>' . $year . '</h5>
	</div>';
	echo '<div class="rightside">';
	if ($education) {
	echo '<h4>Education</h3>
	<div class="staff-list-detail">';
	foreach( $education as $edu ){
			echo '<p>' . $edu['name'] . '</p>';
		}
	echo '</div>';
	}
	if ($bio) {
	echo '
	<h4>Biography</h3>
	<div class="staff-list-detail">' . $bio . '</div>';
	}
	echo '</div>
</div>
</div>
';
		$num++;
	}
	echo '</div>';
}


if( have_rows('bhsfield_staff') ){
	echo '<h2>Field Staff</h2>
	<div class="staff-list">';
	while ( have_rows('bhsfield_staff') ) { the_row();
		$name = get_sub_field('name');
		$title = get_sub_field('title');
		$image = get_sub_field('image');
		$imageurl = wp_get_attachment_image_url( $image, 'large');
		$imageurl = $image['url'];
		$bio = get_sub_field('bio');
		$education = get_sub_field('education');
		$year = get_sub_field('year');

		echo '<div id="box" class="box" style="background-image: url(' . $imageurl . ')">
<div class="boxout"></div>';
echo '<div class="staff-list-name"><span><strong>' . $name . '</strong></span><br><span>' . $title . '</span><br/><span style="font-size: 0.875rem;">' . $year . '</span></div>';
echo '<div class="boxcontent">';
if ($bio || $education) {
echo '<a href="#boxpopup' . $num . '" class="open-popup-link">';
}
echo '<div class="item"><h4>' . $name . '</h4>';
echo '<h5>' . $title . '</h5>';
echo '<h5>' . $year . '</h5>';
if ($bio || $education) {
echo '<div class="staff-more-info">More Info</div>';
}
echo '</div>';
if ($bio || $education) {
echo '</a>';
}
echo '</div>';
echo '<div id="boxpopup' . $num . '" class="boxpopup mfp-hide">
	<div class="leftside">';
	echo '<img src="' . $imageurl . '">
	<h2>' . $name . '</h2>
	<h5>' . $title . '</h5>
	</div>';
	echo '<div class="rightside">';
	if ($education) {
	echo '<h4>Education</h3>
	<div class="staff-list-detail">';
	foreach( $education as $edu ){
			echo '<p>' . $edu['name'] . '</p>';
		}
	echo '</div>';
	}
	if ($bio) {
	echo '
	<h4>Biography</h3>
	<div class="staff-list-detail">' . $bio . '</div>';
	}
	echo '</div>
</div>
</div>
';
		$num++;
	}
	echo '</div>';
}



?>
<script>
jQuery(document).ready(function( $ ) {
	
$('.open-popup-link').magnificPopup({
						type:'inline',
					});
});
</script>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
