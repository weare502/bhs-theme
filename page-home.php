<?php
/**
 * Template Name: Home Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bhs_construction
 */

get_header();
?>

<?php get_template_part( 'template-parts/slider');?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
<div class="slider">
	<?php 
		if( have_rows('homeslider') ){ ?>
		<div class="homeslider owl-carousel owl-theme">
		<?php while ( have_rows('homeslider') ) { the_row(); 
				$image_or_video = get_sub_field('image_or_video');
				$video_link = get_field('video_link');
				$imageurl = "";
				if ($image_or_video == "Image") {}
					$image = get_sub_field('background_image');
					$imageurl = $image['url'];
				
				
				echo '<div class="slideitem" style="background-image: url(' . $imageurl . ')">';
				echo '<div class="slideoverlay"></div>';
				if ($image_or_video == "Video") {
					echo '
			<video class="videoslide" muted loop autoplay="autoplay">
			<source src="' . get_sub_field('video_file') . '" type="video/mp4">
			Your browser does not support the video tag.
			</video> ';
				}
				echo '<img class="kenburns" src="' . $imageurl . '">';
				echo '<div class="slidecontent">';
				get_template_part( "fragments/homeanimation");
				
				if ($video_link) {
			//echo '<a class="sidebar-video video-play-button-overlay" data-video-id="y-z-D1PJ1cMXs">Play Video</a>';
			echo '<a class="videolink" href="' . $video_link . '">PLAY VIDEO ';
			get_template_part( "fragments/playbutton");
			echo '</a>';
			}
			echo '</div>';
			
			echo '</div>';
				
		}
		
		
				?>
				</div>
				<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
<script>
				$(document).ready(function(){
					var kenburnsflip = true;
					var currentindex = 0;
					$('.homeanimation svg').hide();
					$('.homeslider').owlCarousel({
							loop: true,
							items: 1,
							margin: 0,
							autoplay: true,
							dots: false,
							animateOut: 'fadeOut',
							animateIn: 'fadeIn',
							touchDrag: false,
							mouseDrag: false,
							//autoplaySpeed: 100,
							//autoplayTimeout: 100,
							//animateOut: 'bounceOut',
							//animateIn: 'jackInTheBox',
							//smartSpeed: 1,
							
							/*
							onInitialized : function(current) {
							//$('.homeanimation svg').eq(3).show();
							setTimeout(function() {
								$('.homeanimation svg').hide();
								$('.homeanimation svg').show();
							}, 0);
							}
							*/
							
							onTranslated  : function(event) {
							var inx = event.page.index;
							var itemx = event.item.index;
							var thisitemx = $('.kenburns').eq(itemx);
							var currentitemx = $('.kenburns').eq(currentindex);
							$('.kenburns').not(thisitemx).removeClass("start odd even");
							},
							
							/*
							onChange : function(event) {
							var itemx = event.item.index;
							
							}
							*/
							
							onChanged : function(event) {
							var itemx = event.item.index;
							var items = event.item.count
							var thisitemx = $('.kenburns').eq(itemx);
							var currentitemx = $('.kenburns').eq(currentindex);
							if (itemx) {
							if (itemx >= 0) {
							$('.kenburns').not(currentitemx).toggleClass("start");
							
							if (kenburnsflip == true) {
									$('.kenburns').eq(itemx).removeClass("start odd even");
									$('.kenburns').eq(itemx).addClass("start odd");
							}
							else {
									$('.kenburns').eq(itemx).removeClass("start odd even");
									$('.kenburns').eq(itemx).addClass("start even");
									
									//$('.kenburns').eq(itemx).addClass("start odd");
									//$('.kenburns').addClass("start even");
									//$('.kenburns').eq(itemx).toggleClass("even odd");
								}
								
								kenburnsflip = !kenburnsflip;
								currentindex = itemx;
								
								
							}
							}
							
							}
							
						});
						
						
						//$('.homeanimation svg').show();
						
						setTimeout(function() {
							$('.homeanimation svg').show();
						}, 1000);
						
						jQuery('.videolink').each(function() {
							jQuery(this).magnificPopup({
											type: 'iframe',
											   });
								
						});
						
						
					
					});
					</script>
<?php 
		}
		?>
		
	</div>
	
	<div class="featured-projects">
	<?php
	$featured_projects = get_field('featured_projects');
	foreach($featured_projects as $key => $p) {
				$pid = $p->ID;
				$title = $p->post_title;
				$permalink = get_permalink($pid);
				$projectimage = get_the_post_thumbnail_url( $pid, 'large' );
				echo '<div class="box" style="background-image: url(\'' . $projectimage . '\')">';
				echo '<div class="boxout"></div>';
				echo '<div class="boxcontent"><a href="' . $permalink . '">';
				echo '<div class="item"><h3>' . $title .'</h3><hr></div>';
				echo '</a></div>';
				echo '</div>';
	}
	?>
	</div>
		<?php
		while ( have_posts() ) :
			the_post();

			//get_template_part( 'template-parts/content', 'page' );


		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_template_part( 'template-parts/home-pre-footer-cta');?>
<?php
get_footer();
